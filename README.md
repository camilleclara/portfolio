# Portfolio Project

## A simple portfolio project to show the little and not so little exercices and projects coded as part of the learning process

I mainly used HTML, vanilla JS, and SCSS.

`npm install` at your first use, then

`npm start` to develop your application

`npm run build` to bundle your application. When it's done, deliver your `dist` folder: it contains anything needed.

