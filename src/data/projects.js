export const projects = [
  {
    id: 0,
    title: 'JS Calculator',
    description: 'A Javascript build interface to do simple math ! Also the first DOM manipulation I ever made ...',
    image: '/assets/images/code.jpg',
    techno: 'HTML, CSS, JavaScript',
    page: 'calculator.html',
  },
  {
    id: 1,
    title: 'To Do List',
    description: 'A Javascript build simple and easy to use to do list app. Minimalist but efficient !',
    image: '/assets/images/code.jpg',
    techno: 'HTML, CSS, JavaScript',
    page: 'todolist.html',
  },
  {
    id: 2,
    title: 'Bullet Journal',
    description: 'A Javascript build bullet journal application, to keep track of, well, anything.',
    image: '/assets/images/code.jpg',
    techno: 'HTML, CSS, JavaScript',
    page: 'bujo.html',
  },
  {
    id: 3,
    title: 'IT-UP',
    description: 'This is the project we build during a hackathon. It is meant to be an event creating app for interns at Interface3.',
    image: '/assets/images/code.jpg',
    techno: 'HTML, CSS, JavaScript, PHP',
    page: 'itup.html',
  },
  {
    id: 3,
    title: 'Whale',
    description: '3D Modelisation of a whale',
    image: '/assets/images/code.jpg',
    techno: 'OpenScad',
    page: 'whale.html',
  },


];
