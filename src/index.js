/* TEST LANGUAGES const english = document.getElementById('en');
const french = document.getElementById('fr');
let language = '';
english.addEventListener('click', () => { language = 'english'; console.log(language); });
french.addEventListener('click', () => { language = 'french'; console.log(language); });
if (language === 'english') {
  // sessionstartEN
  const titre = document.getElementById('WHAT');
  titre.innerHTML = '<h1>THIS IS THE TITLE</h1>';
}
if (language === 'french') {
  const titre = document.getElementById('WHAT');
  titre.innerHTML = '<h1>CECI EST UN TITRE</h1>';
}
const titre = document.getElementById('WHAT');
titre.innerHTML = '<h1>CECI EST UN TITRE</h1>';
*/
// const btn = document.getElementById('btn');
// btn.addEventListener('click', () => {
//   Email.send({
//     Host: 'smtp.elasticemail.com',
//     Username: 'camilleberrier@hotmail.com',
//     Password: 'ce4b592b-7b67-4562-87b2-53e32c40b9ce',
//     To: 'camilleberrier@hotmail.com',
//     From: 'you@isp.com',
//     Subject: 'This is the subject',
//     Body: 'And this is the body',
//   }).then(
//     (message) => alert(message),
//   );
// });
import { projects } from './data/projects';

const hello = document.getElementById('hello');
const upArrow = document.getElementById('upArrow');

const about = document.getElementById('about');
const tellabout = document.getElementById('tellabout');
const navabout = document.getElementById('navabout');

const portfolio = document.getElementById('portfolio');
const tellportfolio = document.getElementById('tellportfolio');
const navportfolio = document.getElementById('navportfolio');

const contact = document.getElementById('contact');
const tellcontact = document.getElementById('tellcontact');
const navcontact = document.getElementById('navcontact');


// CACHER LE BOUTON UP ARROW
hello.addEventListener('mouseenter', () => {
  upArrow.style.visibility = 'hidden';
});
hello.addEventListener('mouseleave', () => {
  upArrow.style.visibility = 'visible';
});
// NAVBAR UNDERLIGNMENT CONTROL
function isAnyPartOfElementInViewport(el) {
  const rect = el.getBoundingClientRect();
  // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
  const windowHeight = (window.innerHeight || document.documentElement.clientHeight);
  const windowWidth = (window.innerWidth || document.documentElement.clientWidth);

  const vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
  const horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

  return (vertInView && horInView);
}
document.addEventListener('scroll', () => {
  if (isAnyPartOfElementInViewport(tellabout)) {
    navabout.style.textDecoration = 'underline';
  } else {
    navabout.style.textDecoration = 'none';
  }
  if (isAnyPartOfElementInViewport(tellportfolio)) {
    navportfolio.style.textDecoration = 'underline';
  } else {
    navportfolio.style.textDecoration = 'none';
  }
  if (isAnyPartOfElementInViewport(tellcontact)) {
    navcontact.style.textDecoration = 'underline';
  } else {
    navcontact.style.textDecoration = 'none';
  }
});

// INJECTION DU PORTFOLIO
const divContainer = document.getElementById('injectionDiv');

projects.forEach((project) => {
  const divProject = document.createElement('div');
  divProject.className += 'grid-item';
  const image = document.createElement('img');
  image.src = './assets/images/code.jpg';
  // Animation au hover
  divProject.className += ' retract-img';
  divProject.append(image);
  divContainer.append(divProject);
});

// DISPLAY RESUME ON CLICK
const modal = document.getElementById('myModal');
const btn = document.getElementById('displayBtn');
btn.onclick = function () {
  modal.style.display = 'block';
};
const closeBtn = document.getElementById('closeBtn');
closeBtn.onclick = function () {
  modal.style.display = 'none';
};
